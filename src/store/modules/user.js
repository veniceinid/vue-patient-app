import * as types from '../mutation-types'

const state = {
    userName: 'frontdesk',
    firstName: 'Nebula',
    lastName: 'User',
    status: 'loggedIn'
}

const getters = {
    getUserName: state => state.userName,
    getFirstName: state => state.firstName,
    getLastName: state => state.lastName,
    isLogin: state => (state.status === 'loggedIn')
}

const actions = {
    logout ({commit, state}, userName) {
        commit(types.LOGOUT)
    }
}

const mutations = {
    [types.LOGOUT] (state, {userName}) {
        state.status = 'loggedOut'
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}