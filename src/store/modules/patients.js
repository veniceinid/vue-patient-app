import patientService from '../../api/patient-service'
import * as types from '../mutation-types'

const state = {
    patients: [],
    filter: {field: 'firstName', value: ''}
}
const toLower = text => {
    return text.toString().toLowerCase();
}

const getters = {
    allPatients: state => state.patients,
    filterPatients: state => {
        if (state.filter.value) {
            if (state.filter.field === 'id') {
                return state.patients.filter(item => item.id === Number(state.filter.value));                                
            }            
            else if (state.filter.field === 'firstName') {
                return state.patients.filter(item => toLower(item.firstName).includes(toLower(state.filter.value)));
            } else if (state.filter.field === 'lastName') {
                return state.patients.filter(item => toLower(item.lastName).includes(toLower(state.filter.value)));                
            }
        }
        return state.patients;
    }
}

const actions = {
    getAllPatients({ commit }) {
        patientService.getAllPatients(patients => {
            commit(types.INIT_PATIENTS, {patients})            
        })
    },

    addPatient({commit}, patient) {
        if (!patient.id) {
            patient.id = (state.patients.length + 1);        
            patientService.addPatient(patient);    
        } else {
            console.log("Patient already exists in the system: " + patient.firstName + " " + patient.lastName + " (" + patient.id +  ")");            
        }
    }
}

const mutations = {
    [types.INIT_PATIENTS] (state, {patients}) {
        state.patients = patients;
    },

    [types.APPLY_FILTER] (state, filter) {
        state.filter = filter;
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}