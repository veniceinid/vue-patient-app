export const INIT_PATIENTS = 'INIT_PATIENTS'
export const REMOVE_PATIENT = 'REMOVE_PATIENT'
export const LOGOUT = 'LOGOUT'
export const APPLY_FILTER = 'APPLY_FILTER'