import Vue from 'vue'
import Router from 'vue-router'
import PatientSearch from '@/components/PatientSearch'
import PatientDetail from '@/components/PatientDetail'
import PatientAdd from '@/components/PatientAdd'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PatientSearch',
      component: PatientSearch
    },
    {
      path: '/add',
      name: 'PatientAdd',
      component: PatientAdd
    },
    {
      path: '/patient/:id',
      name: 'PatientDetail',
      component: PatientDetail
    }
  ]
})
